import json
import socket
import sys
import math
import os
import time
import pickle
import numpy as np

from priodict import priorityDictionary


generateResultPNG = True

try:
    import matplotlib
    matplotlib.use("Agg")  # or whichever backend you wish to use
    import matplotlib.pyplot as plt
except ImportError:
    generateResultPNG = False
    
class Memory:
    def __init__(self):
        self.ticks = []
        
        self.cars = {}
        
    def addCar(self, carName):
        self.cars[carName] = {"speeds" : [], "angles" : []}

    def addTick(self, tick):
        self.ticks.append(tick)
        
    def memorizeCar(self, carName, speed, angle):
        car = self.cars[carName]
        car["speeds"].append(speed)
        car["angles"].append(angle)
        
    def savePNG(self):
        if generateResultPNG:
            plt.figure(figsize=(16, 16))
            
            for carName in self.cars:
                car = self.cars[carName]
                speeds = car["speeds"]
                angles = car["angles"]
                plt.plot( self.ticks, speeds, self.ticks, angles, '-' )
                plt.savefig(os.path.join(os.path.dirname(__file__), "result_"+carName+".png"))
                
            
                with open(os.path.join(os.path.dirname(__file__), "car_"+carName+".txt"), 'w') as outfile:
                    json.dump(car, outfile)

def Dijkstra(graph,start,end=None):
    
    final_distances = {}	# dictionary of final distances
    predecessors = {}	# dictionary of predecessors
    estimated_distances = priorityDictionary()   # est.dist. of non-final vert.
    estimated_distances[start] = 0

    for vertex in estimated_distances:
        final_distances[vertex] = estimated_distances[vertex]
        if vertex == end: break

        for edge in graph[vertex]:
            path_distance = final_distances[vertex] + graph[vertex][edge]
            if edge in final_distances:
                if path_distance < final_distances[edge]:
                    raise ValueError, "Dijkstra: found better path to already-final vertex"
            elif edge not in estimated_distances or path_distance < estimated_distances[edge]:
                estimated_distances[edge] = path_distance
                predecessors[edge] = vertex

    return (final_distances,predecessors) 

class Graph:
    def __init__(self):
        self.vertices = {}
        
    def add_vertex(self, name, edges):
        self.vertices[name] = edges
    
    def shortest_path(self, start, finish):
        final_distances, predecessors = Dijkstra(self.vertices,start,finish)
        #print final_distances
        path = []
        while 1:
            path.append(finish)
            if finish == start: break
            finish = predecessors[finish]
        path.reverse()
        return final_distances[path[-1]], path
        
    def __str__(self):
        return str(self.vertices)

class Piece(object):
    def __init__(self):
        self.next = None
        self.prev = None
        self.isCurve = False
        self.switch = False
        self.index = -1
        pass
    
    def setAdjacent(self, prev, next):
        self.prev = prev
        self.next = next
    
    def getSafeSpeed(self):
        return 4.0
        
class Straight(Piece):
    def __init__(self, length, switch):
        super(Straight, self).__init__()
        self.length = float(length)
        self.switch = (switch == True)
        self.isCurve = False
        #print "New track piece: Straight(" + str(length) + ", " + str(switch) + ")"
        pass        
        
    def getSafeSpeed(self):
    
        if not self.next.isCurve:
            if not self.next.next.isCurve:
                if not self.next.next.next.isCurve:
                    if not self.next.next.next.next.isCurve:
                        return 20.0
                    return 14.0
                return 12.0
            return 10.0
        if self.next.isCurve:
            if self.length < 100:
                return 8.0
            return 9.0
        return 9.0
        
        
class Curve(Piece):
    def __init__(self, radius, angle, switch):
        super(Curve, self).__init__()
        self.radius = radius
        self.angle = angle
        self.switch = (switch == True)
        self.length = math.fabs(self.angle) * 2.0 * math.pi * self.radius / 360.0

        self.isCurve = True
        #print "New track piece: Curve(" + str(radius) + ", " + str(angle) + ", " + str(self.length) + ")"
        pass    
    
    def laneLength(self, laneDistance):
        k = 1
        if self.angle < 0:
            k = -1
        return math.fabs(self.angle) * 2.0 * math.pi * (self.radius - k * laneDistance) / 360.0
    
    def getSafeSpeedBasedOnAngle(self):
        if math.fabs(self.angle) > 40:
            if self.length > 60:
                return 6.50
            return 4.55
        if self.length < 80:
            return 10.0
        return 8.0
    
    def getSafeSpeed(self):
        if self.next.isCurve:
            if self.next.next.isCurve:
                return self.getSafeSpeedBasedOnAngle() * 0.8
            return self.getSafeSpeedBasedOnAngle() * 0.9
        return self.getSafeSpeedBasedOnAngle()
        
        
class Car(object):
    def __init__(self, data):
        self.id = "?"
        if "id" in data:
            if "name" in data["id"]:    
                self.id = data["id"]["name"]
                print "Registered car " + self.id
                
        self.length = 40.0
        self.width = 20.0
        self.guideFlagPosition = 10.0
        self.currentTrackPiece = None
        self.angle = 0.0
        
        self.angularVelocity = 0.0
             
        self.piecePosition = {}
        self.lap = 0
        self.speed = 0.0
        self.currentLane = None
        self.usingTurbo = False
        self.turboAvailable = False
        self.turboFactor = 1.0
        self.turboDuration = 0 #ticks
        
        if "dimensions" in data:
            if "length" in data["dimensions"]:
                self.length = data["dimensions"]["length"]
            if "width" in data["dimensions"]:
                self.width = data["dimensions"]["width"]
            if "guideFlagPosition" in data["dimensions"]:
                self.guideFlagPosition = data["dimensions"]["guideFlagPosition"]
        
        self.momentOfInertia = (self.length * self.length + self.width * self.width) / 12.0

        #print "New track piece: Curve(" + str(radius) + ", " + str(angle) + ")"
        pass
        
    # Tells you which is going to be next tick acceleration, when you now press throttle = T
    def vel(self, T):
        return 0.2 * (T - 0.1 * self.speed)
        
    # Tells you which is going to be next tick speed, when you now press throttle = T
    def nextSpeed(self, T):
        return self.speed + self.vel(T)
        
    # Tells you which is going to be next tick angle
    def nextAngle(self, T):
        
        angV = math.radians(self.angularVelocity)
        angV = angV + self.nextAngleAcc(T, self.nextSpeed(T))
        
        return math.degrees(math.radians(self.angle) + angV)
        
    # Tells you which is going to be next tick angle
    def nextAngleAcc(self, T, speed):
        angA = 0.0
        radius = 0.0
        
        if self.currentTrackPiece.isCurve:
            radius = self.currentTrackPiece.radius - self.currentLane['distanceFromCenter']
        
        if radius > 0.0:
            '''
            centripetal = speed*speed - 28.8 / radius
            r = self.length * 0.5 - self.guideFlagPosition
            torque = centripetal * r * math.cos(math.radians(self.angle))
            
            angA = torque / self.momentOfInertia
            '''
            
            
            
        
        if speed < math.sqrt(28.8):
            angA = 0.0
            
        return angA
        
    # Angle after n ticks, if throttle is constant T, or list, which is at least length of ticks
    def angleAfterTicks(self, T, ticks):
        angV = math.radians(self.angularVelocity)
        result = self.angle
        if isinstance(T, list):
            assert (len(T) == ticks)
        
        for t in range(0,ticks):
            speed = self.speedAfterTicks(T, t)
            if isinstance(T, list):
                angV = angV + self.nextAngleAcc(T[t], speed)
                result = result + math.degrees(angV)
            else:
                angV = angV + self.nextAngleAcc(T, speed)
                result = result + math.degrees(angV)
                
            print "In future: %s, %s" % (speed, result)
            
        
        return result        
    
    # Speed after n ticks, if throttle is constant T, or list, which is at least length of ticks
    def speedAfterTicks(self, T, ticks):
        result = self.speed
        if isinstance(T, list):
            assert (len(T) == ticks)
        
        for t in range(0,ticks):
            if isinstance(T, list):
                result = result + 0.2 * (T[ticks] - 0.1 * result)
            else:
                result = result + 0.2 * (T - 0.1 * result)
            
        
        return result
    

class Track(object):
    def __init__(self, data, bot):
        self.pieces = []
        self.cars = {}
        self.bot = bot
        if "track" in data:
            if "pieces" in data['track']:
                self.initPieces(data['track']["pieces"])
            else:
                print "Unable to find track from data!"
            if "lanes" in data['track']:
                self.initLanes(data['track']["lanes"])
            else:
                print "Unable to find lanes from data!"
        
        if len(self.pieces) == 0:
            print "Unable to find track from data!"
            
        if "cars" in data:
            self.initCars(data["cars"])
        else:
            print "Unable to find cars from data!"
            
        self.graph = None
            
        
    def initPieces(self, pieces):
        self.pieces = []
        
        for piece in pieces:
            if "length" in piece:
                length = piece["length"]
                switch = False
                if "switch" in piece:
                    switch = piece["switch"]
                newPiece = Straight(length, switch)
                self.pieces.append(newPiece)
                newPiece.index = self.pieces.index(newPiece)
          
        
            if "radius" in piece:
                radius = piece["radius"]
                angle = 0
                if "angle" in piece:
                    angle = piece["angle"]
                switch = False
                if "switch" in piece:
                    switch = piece["switch"]                    
                newPiece = Curve(radius, angle, switch)
                self.pieces.append(newPiece)
                newPiece.index = self.pieces.index(newPiece)
                
        i = 0
        count = len(self.pieces)
        for i in range(0, count):
            prev = (i - 1) % count
            next = (i + 1) % count
            self.pieces[i].setAdjacent(self.pieces[prev], self.pieces[next])
        
        pass 

    def initCars(self, cars):
        self.cars = {}
        
        for car in cars:
            newCar = Car(car)
            self.cars[newCar.id] = newCar
            
            if self.bot and self.bot.memory:
                self.bot.memory.addCar(newCar.id)
        pass
        
    def getRanking(self, whatCar):
        distances = []
        for carID in self.cars:
            car = self.cars[carID]
            distances.append((carID, car.pieceIndex + car.lap * len(self.pieces)))
        
        # Sort cars by distance
        sortedByDistance = sorted(distances, key=lambda car: car[1])
        sortedByDistance.reverse()
        for x in range(0, len(distances)):
            d = sortedByDistance[x]
            if whatCar == d[0]:
                return x + 1
        
    def initLanes(self, lanes):
        self.lanes = lanes
        print("lanes: {0}".format(lanes))
        pass        
    
    def setTurbo(self, car, value):
        id = None
        if "name" in car:
            id = car["name"]
                    
        if not id or not id in self.cars:
            print "Ignoring ID = %s " % id
            return

        car = self.cars[id]
        
        car.usingTurbo = value
        
        if value:
            car.turboAvailable = False
        
    def updateCars(self, data, delta):
        if self.bot and self.bot.memory:
            self.bot.memory.addTick(self.bot.gameTick)
        
        for eCar in data:
            id = None
            if "id" in eCar:
                if "name" in eCar["id"]:
                    id = eCar["id"]["name"]
                    
            if not id or not id in self.cars:
                print "Ignoring ID = " + str(id)
                continue

            car = self.cars[id]
            if 'angle' in eCar:
                car.angularVelocity = eCar['angle'] - car.angle
                car.angle = eCar['angle']
            
            if 'piecePosition' in eCar:
                lastPosition = 0.0
                lastIndex = 0
                if "pieceIndex" in car.piecePosition:
                    lastPosition = car.piecePosition['inPieceDistance']
                    lastIndex = car.piecePosition['pieceIndex']
               
                car.piecePosition = eCar['piecePosition']
                car.currentTrackPiece = self.pieces[car.piecePosition['pieceIndex']]
                
                car.currentLane = self.lanes[int(car.piecePosition['lane']['startLaneIndex'])]
                
                if "pieceIndex" in car.piecePosition:
                    if car.piecePosition['pieceIndex'] == lastIndex:
                        # We handle lagging connection with delta
                        car.speed = (car.piecePosition['inPieceDistance'] - lastPosition) / 1
                    else:
                        prev = car.currentTrackPiece.prev
                        if prev.switch:
                            if not prev.isCurve:
                                pass
                            else:
                                pass
                        else:
                            if not prev.isCurve:
                                car.speed = (car.piecePosition['inPieceDistance'] + (prev.length - lastPosition)) / 1
                            else:
                                car.speed = (car.piecePosition['inPieceDistance'] + (prev.laneLength(self.lanes[car.piecePosition['lane']['startLaneIndex']]['distanceFromCenter']) - lastPosition)) / 1

                                       
            if 'lap' in eCar:
                car.lap = eCar['lap']
                
            if self.bot and self.bot.memory:
                self.bot.memory.memorizeCar(id, car.speed, car.angle)
                            
        pass
    
    def buildGraph(self):
        graph = Graph();
        i = 0
        pieceCount = len(self.pieces)
        for i in range(0, pieceCount):
            
            l = 0
            laneCount = len(self.lanes)
            piece = self.pieces[i]
            nextPiece = piece.next
           
            
            for l in range(0, laneCount):
                neighbours = {}
                
                if not piece.switch:
                    if not piece.isCurve:
                        neighbours[nextPiece.index * 10 + l] = piece.length
                        #print str(piece.index*10 + l) + " => " + str(nextPiece.index * 10 + l)
                    else:
                        neighbours[nextPiece.index * 10 + l] = piece.laneLength(self.lanes[l]['distanceFromCenter'])
                        #print str(piece.index*10 + l) + " => " + str(nextPiece.index * 10 + l)
                else: # We have a switch
                    # We assume we can switch from any lane to any lane, or stay on the lane
                    nl = 0
                    
                    for nl in range(0, laneCount):
                        # Penalty for changing lane
                        change = 0.5
                        if nl == l:
                            change = 0.0
                        if not piece.isCurve:
                            neighbours[nextPiece.index * 10 + nl] = piece.length + change
                            #print str(piece.index*10 + l) + " => " + str(nextPiece.index * 10 + nl)
                        else:
                            neighbours[nextPiece.index * 10 + nl] = piece.laneLength(self.lanes[l]['distanceFromCenter']) + change
                            #print str(piece.index*10 + l) + " => " + str(nextPiece.index * 10 + nl)
                            
                graph.add_vertex(i*10 + l, neighbours)
                #print("graph.add_vertex({0}, {1})".format(i*10 + l, neighbours))
                
        self.graph = graph
        #print graph
        
    def shortestPath(self, curPieceID, curlaneID, toPieceID, tolaneID):
       
        if not self.graph:
            self.buildGraph()
        curPieceID = curPieceID % len(self.pieces)
        toPieceID = toPieceID % len(self.pieces)
        curlaneID = curlaneID % len(self.lanes)
        shortest = []
        if tolaneID != -1:
            tolaneID = tolaneID % len(self.lanes)
            
            distance, shortest = self.graph.shortest_path(curPieceID * 10 + curlaneID,toPieceID * 10 + tolaneID)
            print distance, shortest
        else:
            nl = 0
            smallest = sys.maxsize
            for nl in range(0, len(self.lanes)):
                currID = curPieceID * 10 + curlaneID
                targetID = toPieceID * 10 + nl
                #print currID, targetID
                distance, path = self.graph.shortest_path(currID, targetID)
                # Find shortest path, but prefer the current lane if ties
                if distance < smallest:
                    smallest = distance
                    shortest = path

        #print path
        resultPath = []
        lastLaneIndex = curlaneID
        for node in shortest:
            pieceIndex = int(math.floor(node / 10))
            laneIndex = int(int(node) % 10)
           
            resultPath.append([pieceIndex, laneIndex])
            
        return resultPath
        
    def shortestPathAsRoute(self, curLaneID, shortestPath):
        
        resultPath = []
        
        lastLaneIndex = curLaneID
        for node in shortestPath:
            pieceIndex = node[0]
            laneIndex = node[1]
          
            if laneIndex == lastLaneIndex:
                resultPath.append("Stay")
            elif laneIndex < lastLaneIndex:
                resultPath.append("Left")
            elif laneIndex > lastLaneIndex:
                resultPath.append("Right")
                
            lastLaneIndex = laneIndex
            
        return resultPath        

        
    def getCar(self, id):
        
        if not id in self.cars:
            return None
        return self.cars[id]

class Brain(object):
    def __init__(self, track, bot, mycar):
        self.track = track
        self.bot = bot
        self.car = mycar
        self.switchedLanes = True
        
        self.maxAngle = 0.0
        
        
    def estimateRadii(self, route, ahead = 3):
        result = []
        
        piece = self.car.currentTrackPiece
        
        for i in range(0, ahead):
            if piece.isCurve:
                result.append(piece.laneLength(self.track.lanes[route[i][1]]['distanceFromCenter']))
            else:
                result.append(1000)
            piece = piece.next
            
        return result

    def closestCarsAhead(self, distanceLimit = 100):
        result = []
        
        piece = self.car.currentTrackPiece
        mylane = int(self.car.piecePosition['lane']['startLaneIndex'])
        for carID in self.track.cars:
            car = self.track.cars[carID]
            if car == self.car:
                continue
            
            distance = 0.0
            angle = car.angle
            lane = int(car.piecePosition['lane']['startLaneIndex'])
            speed = car.speed
            
            if mylane != lane:
                # Ignoring cars in different lanes
                continue
            
            if car.currentTrackPiece == piece: # Car is on same piece!
                # Distance < 0, if car is behind us
                distance = self.car.piecePosition['inPieceDistance'] - car.piecePosition['inPieceDistance']
                if distance < 0:
                    continue
               
            elif car.currentTrackPiece == piece.prev: # Car is behind us!
                continue
                
            elif car.currentTrackPiece == piece.next: # Car is ahead us!
                distance = car.piecePosition['inPieceDistance']
                if not piece.isCurve:
                    distance = distance + piece.length - self.car.piecePosition['inPieceDistance']
                else:
                    distance = distance + piece.laneLength(self.track.lanes[mylane]['distanceFromCenter']) - self.car.piecePosition['inPieceDistance']
            else:
                continue
                
            if distance > distanceLimit:
                continue
            result.append([distance, speed, angle, lane])
            
           
        # Sort cars by distance
        sortedByDistance = sorted(result, key=lambda car: car[0])
        
        # Return only the closest ones (amount)
        sortedByDistance.reverse()
        return sortedByDistance

    def closestCars(self, amount = 2):
        result = []
        
        piece = self.car.currentTrackPiece
        mylane = int(self.car.piecePosition['lane']['startLaneIndex'])
        for carID in self.track.cars:
            car = self.track.cars[carID]
            if car == self.car:
                continue
            
            distance = 0.0
            angle = car.angle
            lane = int(car.piecePosition['lane']['startLaneIndex'])
            speed = car.speed
            
            if mylane != lane:
                # Ignoring cars in different lanes
                continue
            
            if car.currentTrackPiece == piece: # Car is on same piece!
                # Distance < 0, if car is behind us
                distance = self.car.piecePosition['inPieceDistance'] - car.piecePosition['inPieceDistance']
               
            elif car.currentTrackPiece == piece.prev: # Car is behind us!
                distance = self.car.piecePosition['inPieceDistance']
                if not piece.prev.isCurve:
                    distance = distance + piece.length - car.piecePosition['inPieceDistance']
                else:
                    distance = distance + piece.laneLength(self.track.lanes[lane]['distanceFromCenter']) - car.piecePosition['inPieceDistance'] 
                distance = -distance
                
            elif car.currentTrackPiece == piece.next: # Car is ahead us!
                distance = car.piecePosition['inPieceDistance']
                if not piece.isCurve:
                    distance = distance + piece.length - self.car.piecePosition['inPieceDistance']
                else:
                    distance = distance + piece.laneLength(self.track.lanes[mylane]['distanceFromCenter']) - self.car.piecePosition['inPieceDistance']
            else:
                continue
                
            result.append([distance, speed, angle, lane])
        if len(result) <= amount:
            return result
            
        # Sort cars by distance
        sortedByDistance = sorted(result, key=lambda car: car[0])
        
        # Return only the closest ones (amount)
        sortedByDistance.reverse()
        return sortedByDistance[0:amount]
              
    def decide(self, deltaTime):
        if not self.bot:
            print "Bot was not set!"
            return
            
        if not self.track:
            print "Track was not set!"
            return
            
        if not self.car:
            print "Car was not set!"
            return
           
        # Get the current track piece
        car = self.car
        
        #print str(car.speed) + ", " + str(car.angle)

        curPiece = car.currentTrackPiece
        # Safe undervalue
        # It seems that car crashes if it's drifting angle exceeds 59.9 degrees
        # 0.655 value is limit constant that car runs Keimola track without crashing (lap speed 8.90 s)
        # Hardly optimal for overall track performance
        #throttle = 0.602
        throttle = 0.0
        calculateRoute = True
        useModularThrottleSpeed = True
        useNeuralNetwork = False
        useBumpingToOtherCars = True
        index = curPiece.index
        curLane = int(car.piecePosition['lane']['startLaneIndex'])
        nextPiece = curPiece.next
        
        #print self.bot.gameTick, index, curLane
                    
        if(int(car.piecePosition['lane']['startLaneIndex']) != int(car.piecePosition['lane']['endLaneIndex'])):
            self.switchedLanes = True
            
        if calculateRoute:
            # Get shortest path from this piece to 5 pieces front
            path = self.track.shortestPath(index, curLane, index + 30, -1) # toLane = -1 means whatever lane, as long it is shortest
            # Convert to instructions
            route = self.track.shortestPathAsRoute(curLane, path)
                
            if (len(route) > 2 and route[2] != "Stay") and self.switchedLanes and self.bot.gameTick > 3:
                # Switch to shortest route
                #print "Switching to lane " + route[2]
                self.bot.switch(route[2])
                self.switchedLanes = False
        
        if useModularThrottleSpeed:
           

            targetSpeed = nextPiece.getSafeSpeed()
                        
            if car.speed - targetSpeed < -0.07:
                if car.speed - targetSpeed < -1.0:
                    throttle = 1.0
                elif car.speed - targetSpeed < -0.1:
                    throttle = 0.9
            elif car.speed - targetSpeed > 0.07:
                if car.speed - targetSpeed > 0.5:
                    throttle = 0.0
                elif car.speed - targetSpeed > 0.1:
                    throttle = 0.1
            else:
                throttle = targetSpeed * 0.1
                     
            threshold = 2.10
            if car.angularVelocity > threshold and car.angle > 0 or car.angularVelocity < -threshold and car.angle < 0:
                throttle = 0.3
                                        
            if math.fabs(car.angle) > 57:
                throttle = 0.0
             
            
            
            estimateSpeed = car.nextSpeed(throttle)
            estimateAngle = car.nextAngle(throttle)
        
        if useBumpingToOtherCars:
            if len(self.closestCarsAhead(50)) > 0:
                throttle = 1.0
            threshold = 2.10
            
            if car.angularVelocity > threshold and car.angle > 0 or car.angularVelocity < -threshold and car.angle < 0:
                throttle = 0.3
                                        
            if math.fabs(car.angle) > 57:
                throttle = 0.0
        
        if useNeuralNetwork:
            # fysiikan testausta:
            tick=self.bot.gameTick;

            # Vadim's Neural Network algorithm.
            # Weight matrices stored in 
            #   Matrix1_new,
            #   Matrix2_new and
            #   Matrix3_new.
            # Skript writes to file "files_/memory"+str(ind) previous parameters to use them
            # in the network.

            # Sigmoid for the NN:
            def sigm(x):
                return 1.0/(1+math.exp(-x));
            def sigmoid(nparray):
                a=np.matrix([[]]);
                for i in nparray.T:
                    a=np.concatenate((a,np.matrix([[sigm(float(i))]])),1);
                return a;

            # Load weight matrices:
            with open(os.path.join(os.path.dirname(__file__), "files_", "SuperMatrix1"), 'rb') as f:
                M1 = np.asmatrix(pickle.load(f))
            with open(os.path.join(os.path.dirname(__file__), "files_", "SuperMatrix2"), 'rb') as f:
                M2 = np.asmatrix(pickle.load(f))
            with open(os.path.join(os.path.dirname(__file__), "files_", "SuperMatrix3"), 'rb') as f:
                M3 = np.asmatrix(pickle.load(f))

            
            # The parameters fed into the NN:
            
            
            
            with open(os.path.join(os.path.dirname(__file__), "files_", "memory"), 'rb') as f:
                memory = pickle.load(f)
            
            with open(os.path.join(os.path.dirname(__file__), "files_", "lastpos_index"),'rb') as f:
                (last_pos,last_index)=pickle.load(f)

            with open(os.path.join(os.path.dirname(__file__), "files_", "lastpos_index"),'wb') as f:
                pickle.dump((car.piecePosition['inPieceDistance'],curPiece.index),f)
                
                
            newDelta=car.speed
            newmemory=[]
            newmemory.append([0,car.angle])
            for k in range(min(19,len(memory))):
                newmemory.append([newDelta+memory[k][0],memory[k][1]])

            with open(os.path.join(os.path.dirname(__file__), "files_", "memory"), 'wb') as f:
                pickle.dump(newmemory,f)
            
            d1=curPiece.length-car.piecePosition['inPieceDistance']
            d2=d1+nextPiece.length
            d3=d2+nextPiece.next.length
            rad=[]
            for i in range(4):
                rad.append(1000)
            
            def getRadius(n): # THIS IS TO BE REPLACED BY PAULI'S BETTER FUNCTION
                r=1000
                if n==0 and curPiece.isCurve:
                    r=curPiece.radius
                elif n==1 and nextPiece.isCurve:
                    r=nextPiece.radius
                elif n==1 and nextPiece.next.isCurve:
                    r=nextPiece.next.radius

                return r
            
            for k in range(4):
                R=(k+1)*40
                if d1>R and curPiece.isCurve:
                    rad[k]=getRadius(0)
                elif d1<R and d2>=R and nextPiece.isCurve:
                    rad[k]=getRadius(1)
                elif d2<R and nextPiece.next.isCurve:
                    rad[k]=getRadius(2)
                
            Neuro_in=[car.angle,
                      getRadius(0),
                      memory[0][0]+newDelta,
                      memory[1][0]+newDelta,
                      memory[5][0]+newDelta,
                      memory[9][0]+newDelta,
                      memory[19][0]+newDelta,
                      memory[0][1],  
                      memory[1][1],  
                      memory[5][1],
                      memory[9][1],
                      memory[19][1],
                      rad[0],
                      rad[1],
                      rad[2],
                      rad[3],
                      0, #angle
                      0, #speed
                      0, #distance
                      0,
                      0,
                      0]
            
            # the last six are the 
            # velocities, angles and distances 
            # of the two closest cars
            # PAULI'S  FUNCTION TO BE USED TO GET THEM

            # That makes 21 parameters....

            # Parameters are normalized to mean=0 and (min,max)=(-1,1):
            Min=[-60,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  -60,
                  -60,
                  -60,
                  -60,
                  -60,
                  0,
                  0,
                  0,
                  0,
                  -60,
                  0,
                  -200,
                  -60,
                  0,
                  -200]
            Max=[60,
                 1000,
                 10,
                 10,
                 10,
                 10,
                 10,
                 60,
                 60,
                 60,
                 60,
                 60,
                 1000,
                 1000,
                 1000,
                 1000,
                 60,
                 10,
                 200,
                 60,
                 10,
                 200]


            Mid=[]
            for k in range(len(Min)):
                Mid.append(0.5*(Max[k]+Min[k]))
            for j in range(len(Neuro_in)):
                Neuro_in[j]=(Neuro_in[j]-Mid[j])/(0.5*(Max[j]-Min[j]))

            Neuro0=np.asmatrix(Neuro_in)        
            Neuro0_t=np.concatenate((Neuro0,np.matrix('1')),1)
            Neuro1=sigmoid(Neuro0_t*M1);
            Neuro1_t=np.concatenate((Neuro1,np.matrix('1')),1)
            Neuro2=sigmoid(Neuro1_t*M2)
            Neuro2_t=np.concatenate((Neuro2,np.matrix('1')),1)
            Neuro_out=float(sigmoid(Neuro2_t*M3));
            # ouput:
            throttle=Neuro_out # it is here that we want to take turbo into account!
            
            if tick<25:
                throttle=1
            c=0
            tracklength=0
            for k in self.track.pieces:
                tracklength=tracklength+k.length
            print "throttle", throttle
            print "speed", car.speed
            print "angle", car.angle
            print "index", curPiece.index
            
        #print car.speed, throttle, car.angle, car.angularVelocity

        # Truncate throttle, so we don't get disqualified for wrong throttle values
        if throttle < 0.0:
            throttle = 0.0
        if throttle > 1.0:
            throttle = 1.0
           
        if car.turboAvailable and (not nextPiece.prev.isCurve and not nextPiece.isCurve and not nextPiece.next.isCurve) and car.usingTurbo == False:
            car.usingTurbo = True
            self.bot.turbo()
            pass
                 
        # Just throttle with maximum safe limit
        self.bot.throttle(throttle)
                    
        pass          

class MielivaltaBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.brain = None
        self.currentTrack = None
        self.lastGameTick = 0
        self.gameTick = 0
        self.host = "unknown"
        self.memory = Memory()
        
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gameTick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
    
        msg = {
          "botId": {
            "name": self.name,
            "key": self.key
          },
          "trackName": "keimola",
          "carCount": 1
          }
        
        if self.host in ['testserver.helloworldopen.com', 'hakkinen.helloworldopen.com', 'senna.helloworldopen.com', 'webber.helloworldopen.com']:
            return self.msg("joinRace", msg)
        else:
            return self.msg("join", {"name": self.name, "key": self.key})
    
    def turbo(self):
        self.msg("turbo", "PRUM PRUM!")
        
    def throttle(self, throttle):
        self.msg("throttle", throttle)
        
    def switch(self, lane):
        if (lane == "Left" or lane == "Right"):
            self.msg("switchLane", lane)
        
    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()
        
    def on_turbo_available(self, data):
        print("Turbo available: {0}".format(data))
        if self.brain:
            self.brain.car.turboAvailable = True
            self.brain.car.turboFactor = data['turboFactor']
            self.brain.car.turboDuration = data['turboDurationTicks']
            
    def on_turbo_start(self, data):
        print("Turbo start: {0}".format(data))
        if self.currentTrack:
            self.currentTrack.setTurbo(data, True)
    
    def on_turbo_end(self, data):
        print("Turbo ended: {0}".format(data))
        if self.currentTrack:
            self.currentTrack.setTurbo(data, False)
        
    def on_game_init(self, data):
        print("Game init data received")
        #print("init: {0}".format(data))
        
        if "race" in data:
            self.currentTrack = Track(data['race'], self)
        else:
            print "Unable to find race information from data!"
        
        #print self.currentTrack.shortestPath(0, 0, len(self.currentTrack.pieces) - 1, -1)
        #print self.currentTrack.shortestPath(0, 1, len(self.currentTrack.pieces) - 1, 1)

        car = self.currentTrack.getCar(self.name)
        
        #self.switch("Right")
        
        self.brain = Brain(self.currentTrack, self, car)
        
        self.ping()

    def on_car_positions(self, data):
        # Decides what to do, based on data received
        #print("on_car_positions: {0}".format(data))
        delta = int(self.gameTick) - int(self.lastGameTick)
        if delta < 1:
            delta = 1
        self.currentTrack.updateCars(data, delta)
        self.brain.decide(delta)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        if not self.socket:
            return
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type = 'NoType'
            data = None
            tick = -1
            if 'msgType' in msg:
                msg_type = msg['msgType']
            if 'data' in msg:
                data = msg['data']
            if 'gameTick' in msg:
                tick = msg['gameTick']

            if msg_type in msg_map:
                self.gameTick = tick
                msg_map[msg_type](data)
                self.lastGameTick = tick
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = MielivaltaBot(s, name, key)
        bot.host = host
        bot.run()
        if generateResultPNG:
            bot.memory.savePNG()