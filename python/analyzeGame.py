import os, sys
import json
import math
gameTick = 0
lastGameTick = 0

cars = {}
track = None
maxSpeeds = {}

turbos = []

try:
    import numpy as np
    import matplotlib

    matplotlib.use("Agg")  # or whichever backend you wish to use
    import matplotlib.pyplot as plt
except ImportError:
    generateResultPNG = False
    
def pos(deltaTime,dampingRatio,initialPos,initialVel,omega0):

        if dampingRatio < omega0: # underdamped
                omega1 = math.sqrt(omega0**2 - dampingRatio**2) # [176 Taylor]
                b1 = initialPos # [165 Taylor]
                b2 = initialVel/omega1
                amplitude = math.sqrt(b1**2 + b2**2) # [166 Taylor]
                phase = math.asin(b2/amplitude)
                return amplitude * math.exp(-dampingRatio * deltaTime) * math.cos(omega1 * deltaTime - phase) # [176 Taylor]
        
        elif dampingRatio > omega0: # overdamped [177 Taylor]
                rplus = -dampingRatio + math.sqrt(dampingRatio**2 - omega0**2)
                rminus = -dampingRatio - math.sqrt(dampingRatio**2 - omega0**2)
                c2 = (initialVel - initialPos*rplus)/(rminus - rplus)
                c1 = initialPos - c2
                return c1 * math.exp(rplus * deltaTime) + c2 * math.exp(rminus * deltaTime)
        else: # critically damped [178 Taylor]
                c1 = initialPos
                c2 = initialVel + dampingRatio * initialPos
                return c1 * math.exp(-dampingRatio * deltaTime) + c2 * deltaTime * math.exp(-dampingRatio * deltaTime)
    

def calcDamped(deltaTime, equilibriumPos, angularFrequency, dampingRatio, initialPos, initialVel):
    omegaZ = angularFrequency * dampingRatio
    alpha = angularFrequency * math.sqrt(1.0 - dampingRatio*dampingRatio)
    expTerm = math.exp( -omegaZ * deltaTime )
    cosTerm = math.cos( alpha * deltaTime )
    sinTerm = math.sin( alpha * deltaTime )

    c1 = initialPos
    c2 = (initialVel + omegaZ*initialPos) / alpha
    pPos = equilibriumPos + expTerm*(c1*cosTerm + c2*sinTerm)
    pVel = -expTerm*( (c1*omegaZ - c2*alpha)*cosTerm + (c1*alpha + c2*omegaZ)*sinTerm)
   
    return (pPos, pVel)
    
def calcDampedFast(t, a, b, x0, v0):
    x1 = math.exp( -(a * b) * t )*(x0*math.cos( (a * math.sqrt(1.0 - b*b)) * t ) + ((v0 + (a * b)*x0) / (a * math.sqrt(1.0 - b*b)))*math.sin( (a * math.sqrt(1.0 - b*b)) * t ))
    v1 = -math.exp( -(a * b) * t )*( (x0*(a * b) - ((v0 + (a * b)*x0) / (a * math.sqrt(1.0 - b*b)))*(a * math.sqrt(1.0 - b*b)))*math.cos( (a * math.sqrt(1.0 - b*b)) * t ) + (x0*(a * math.sqrt(1.0 - b*b)) + ((v0 + (a * b)*x0) / (a * math.sqrt(1.0 - b*b)))*(a * b))*math.sin( (a * math.sqrt(1.0 - b*b)) * t ))
   
    return (x1, v1)   

def estimateAngles(duration, a, b, x0, v0):
    angles = [x0]
    for t in range(0, duration):
        x1=x0+t*v0
        v1=v0+t*(-w*w*x1-b*v0)
        angles.append(angles[t] + x1)
        x0 = x1
        v0 = v1
    
    return angles
    
'''
C = math.sqrt(9.0 /(32.0 * laneRadius)) * speed * speed - 0.3 * speed
a,b = calcDamped(deltaTime, angularFrequency, dampingRatio, C , C )
'''
class Memory:
    def __init__(self):
        self.ticks = []
        self.cars = {}
        
    def addCar(self, carName):
        self.cars[carName] = {"speeds" : [], "angles" : [], "trackPieces" : [], "velocities" : [0.0], "distancesInPiece" : []}

    def addTick(self, tick):
        self.ticks.append(tick)

    def memorizeCarVelocity(self, carName, velocity):
        car = self.cars["NFC-stepping"]
        car["velocities"].append(velocity)
    
    def memorizeCar(self, carName, speed, angle, index, dst):
        if not carName in self.cars:
            return
        car = self.cars[carName]
        car["speeds"].append(speed)
        car["angles"].append(angle)
        car["trackPieces"].append(index)
        car["distancesInPiece"].append(dst)
        
    def savePNG(self):
        #fig = plt.figure(figsize=(60, 60))
        fig = plt.figure(figsize=(10, 10))
        ticks = self.ticks
        maxt = len(ticks)-1
        print maxt
        ticks = ticks[0:maxt]
        plt.grid(b=True, which='minor', color='gray', linestyle='-')
        plt.grid(b=True, which='major', color='black', linestyle='-')
        
        plt.xticks(np.arange(min(ticks), max(ticks)+1, 60))
        
        plt.minorticks_on()
        #minor_ticks = np.arange(1,10,2)
        for carName in self.cars:
            car = self.cars[carName]

            speeds = car["speeds"]
            angles = car["angles"]
            trackPieces = car["trackPieces"]
            #for t in range(0, maxt):
            #    trackPieces[t] = trackPieces[t]* 0.01
            velocities = car["velocities"]
            print len(velocities)
            dist = car["distancesInPiece"]
            #plt.yticks(np.arange(-10, 50, 0.2))
            #plt.xticks(np.arange(0, max(ticks), 1.0))
            dv = np.zeros(len(angles)) #we know it will be this size
            dv[0:-1] = np.diff(angles) / np.diff(ticks)
            dv = np.roll(dv, 1)
            mint = 0
            for t in range(0, max(ticks)):
                if angles[t] != 0.0:
                    mint = t
                    break
            
            
            da = np.zeros(len(dv)) #we know it will be this size
            da[0:-1] = np.diff(dv) / np.diff(ticks)
            da = np.roll(da, 1)
            
            daa = np.zeros(len(da)) #we know it will be this size
            daaa = np.zeros(len(da)) #we know it will be this size
            speed = speeds[mint]
            #speed = 6.1133994839
            print "speed =", speed
            cAcc = (speed) / 90.0
            print "centripetal Acc =", cAcc
            C = 0.75 * math.sqrt(1.0 /(2.0 * 90.0)) * speed * speed - 0.3 * speed
            print C
            print "First term"
            print C - dv[mint]
            print C - da[mint]
            
            # Angle
            #speed = 6.13352874475
            
            # speed 6.1133994839
            # dampingRatio 0.677480724381
            # angularFrequency 0.0887683113377

            # speed 6.42391670085
            # dampingRatio 0.62072983353
            # angularFrequency 0.0912293165277
            
            # speed 6.42391670085
            # error 0.00341520741184
            # dampingRatio 0.99
            # angularFrequency 0.0709269485793
            
            dampingRatio = 0.1
            
            angularFrequency = 0.1
            
            initialPos = C
            initialVel = C 
            equilibriumPos = 0
            error=88

            '''
            for t in range(mint, maxt):
                deltaTime = (t - mint)

                pos, vel = calcDamped(deltaTime, equilibriumPos, angularFrequency, dampingRatio, initialPos, initialVel)
                
                if t == 0:
                    print pos, vel
                    
                daa[t] = pos
            '''
            
            x0 = 0.0
            v0 = 0.0
            w = 0.085

            b = 0.085
            
            print w,b
            
            i=0
            maxI = 100
            while i<maxI:
                i=i+1
                
                for var in ['w', 'b']:#, 'p', 'v', 'c']:
                    newW=w
                    newB=b
                    if var=='w':
                        newW=w+0.01*(np.random.random()-0.5)
                    elif var=='b':
                        newB=b+0.01*(np.random.random()-0.5)
                                          
                    #w=np.random.random()
                    #b=np.random.random()

                    fix = 0
                    x0 = 0.0
                    v0 = 0.0
                    for t in range(mint, maxt):
                        x1=x0+1*v0
                        v1=v0+1*(-w*w*x1-b*v0)
                        daa[t] = x1
                        daaa[t] = angles[t] + x1
                        x0 = x1
                        v0 = v1
                        if math.fabs(x0 - dv[t]) > 1.0:
                            x0 = dv[t]
                            v0 = da[t]
                            fix = fix + 1
                        
                        
                     
                    newerror=0
                    for t in range(mint, maxt):
                        newerror=fix
                    if newerror<error:
                        error=newerror
                        if var=='w':
                            w=newW
                        elif var=='b':
                            b=newB
                    
                        print "newerror", newerror
                        print "w", w
                        print "b", b
                        
            

            
            '''
            i=0
            maxI = 10000
            while i<maxI:
                i=i+1
                
                for var in ['r', 'f']:#, 'p', 'v', 'c']:
                    newDampingRatio=dampingRatio
                    newAngularFrequency=angularFrequency
                    newInitialPos=initialPos
                    newInitialVel=initialVel
                    if var=='r':
                        newDampingRatio=max(min(dampingRatio+0.001*(np.random.random()-0.5),0.99),0.0)
                    elif var=='f':
                        newAngularFrequency=angularFrequency+0.001*(np.random.random()-0.5)
                    elif var=='p':
                        newInitialPos=initialPos+0.001*(np.random.random()-0.5)
                    elif var=='v':
                        newInitialVel=initialVel+0.001*(np.random.random()-0.5)
                    
                    if error==120 and i>1:
                        dampingRatio=np.random.random()
                        angularFrequency=np.random.random()
                        initialPos=np.random.random()
                        initialVel=np.random.random()
                        pass
                       
                        
                    a = np.zeros(len(angles)) #we know it will be this size

                    for t in range(mint, maxt):
                        deltaTime = (t - mint)
                        a1,b1 = calcDamped(deltaTime, equilibriumPos, newAngularFrequency, newDampingRatio, newInitialPos, newInitialVel)
                        a[t] = a1
                        
                        
                     
                    newerror=0
                    for t in range(mint, maxt):
                        if newerror<error:
                            newerror=newerror+(a[t]-dv[t])**2
                    if newerror<error:
                        error=newerror
                        if var=='r':
                            dampingRatio=newDampingRatio
                        elif var=='f':
                            angularFrequency=newAngularFrequency
                        elif var=='p':
                            initialPos=newInitialPos
                        elif var=='v':
                            initialVel=newInitialVel
                            
                        print "newerror", newerror
                        print "dampingRatio", dampingRatio
                        print "angularFrequency", angularFrequency
                        print "initialPos", initialPos
                        print "initialVel", initialVel           

            for t in range(mint, max(ticks)):
                deltaTime = (t - mint)

                a1,b1 = calcDamped(deltaTime, equilibriumPos, angularFrequency, dampingRatio, initialPos, initialVel)
                daa[t] = a1
                
            '''
            
            fix = 0
            x0 = 0.0
            v0 = 0.0
            for t in range(mint, maxt):
                x1=x0+1*v0
                v1=v0+1*(-w*w*x1-b*v0)
                daa[t] = x1
                daaa[t] = angles[t] + x1
                x0 = x1
                v0 = v1
                if math.fabs(x0 - dv[t]) > 1.0:
                    x0 = dv[t]
                    v0 = da[t]
                    fix = fix + 1
            
            error=0
            for t in range(mint, maxt):
                error=error+(daaa[t]-angles[t])**2
             
            print "--"
            print "speed", speed
            print "error", error
            print "w", w
            print "b", b
            print "Fixes: ", fix

            #plt.plot( ticks, speeds, ticks, angles, ticks, trackPieces, '-' )
            plt.plot(ticks, speeds, ticks, angles, ticks, daaa, '-' )

            plt.savefig(os.path.join(os.path.dirname(__file__), "result_"+carName+".png"))

class Car(object):
    def __init__(self, data):
        self.id = "?"
        if "id" in data:
            if "name" in data["id"]:    
                self.id = data["id"]["name"]
                print "Registered car " + self.id
                
        self.length = 40.0
        self.width = 20.0
        self.guideFlagPosition = 10.0
        
        self.angle = 0.0
        self.piecePosition = {}
        self.lap = 0
        self.speed = 0.0
        self.pieceIndex = -1
        
        if "dimensions" in data:
            if "length" in data["dimensions"]:
                self.length = data["dimensions"]["length"]
            if "width" in data["dimensions"]:
                self.width = data["dimensions"]["width"]
            if "guideFlagPosition" in data["dimensions"]:
                self.guideFlagPosition = data["dimensions"]["guideFlagPosition"]
        
        #print "New track piece: Curve(" + str(radius) + ", " + str(angle) + ")"
        pass 
        
class Piece(object):
    def __init__(self):
        self.next = None
        self.prev = None
        self.isCurve = False
        self.switch = False
        self.index = -1
        pass
    
    def setAdjacent(self, prev, next):
        self.prev = prev
        self.next = next
    
    def getSafeSpeed(self):
        return 4.0
        
class Straight(Piece):
    def __init__(self, length, switch):
        super(Straight, self).__init__()
        self.length = float(length)
        self.switch = (switch == True)
        self.isCurve = False
        print "New track piece: Straight(%s, %s)" % (length, switch)
        pass        
        
    def getSafeSpeed(self):
        if not self.next.isCurve:
            if not self.next.next.isCurve:
                return 12.0
            return 10.0
        if self.next.isCurve:
            if self.length < 100:
                return 8.0
            return 9.0
        return 9.0
        
        
class Curve(Piece):
    def __init__(self, radius, angle, switch):
        super(Curve, self).__init__()
        self.radius = radius
        self.angle = angle
        self.switch = (switch == True)
        self.length = math.fabs(self.angle) * 2 * math.pi * self.radius / 360 
        
        self.isCurve = True
        print "New track piece: Curve(%s, %s, %s)" % (radius, angle, self.length)
        pass    
    
    def laneLength(self, laneDistance):
        k = 1
        if self.angle < 0:
            k = -1
        return math.fabs(self.angle) * 2 * math.pi * (self.radius - k * laneDistance) / 360 
    
    def getSafeSpeed(self):
        if math.fabs(self.angle) > 40:
            if self.length > 60:
                return 6.453
            return 4.55
        return 8.0        
        
class Track(object):
    def __init__(self, data):
        self.pieces = []
        if "track" in data:
            if "pieces" in data['track']:
                self.initPieces(data['track']["pieces"])
            else:
                print "Unable to find track from data!"
            if "lanes" in data['track']:
                self.initLanes(data['track']["lanes"])
            else:
                print "Unable to find lanes from data!"
        
        if len(self.pieces) == 0:
            print "Unable to find track from data!"
            
        
    def initPieces(self, pieces):
        self.pieces = []
        
        for piece in pieces:
            if "length" in piece:
                length = piece["length"]
                switch = False
                if "switch" in piece:
                    switch = piece["switch"]
                newPiece = Straight(length, switch)
                self.pieces.append(newPiece)
                newPiece.index = self.pieces.index(newPiece)
          
        
            if "radius" in piece:
                radius = piece["radius"]
                angle = 0
                if "angle" in piece:
                    angle = piece["angle"]
                switch = False
                if "switch" in piece:
                    switch = piece["switch"]                    
                newPiece = Curve(radius, angle, switch)
                self.pieces.append(newPiece)
                newPiece.index = self.pieces.index(newPiece)
                
        i = 0
        count = len(self.pieces)
        for i in range(0, count):
            prev = (i - 1) % count
            next = (i + 1) % count
            self.pieces[i].setAdjacent(self.pieces[prev], self.pieces[next])
        
        pass   

    def initLanes(self, lanes):
        self.lanes = lanes
        print("lanes: {0}".format(lanes))
        pass 
        
memory = Memory()

def on_gameInit(data):
    print "Analysing " + data['race']['track']['name']
    global track
    track = Track(data['race'])
    
    dcars = data['race']['cars']
    
    for car in dcars:
        newCar = Car(car)
        cars[newCar.id] = newCar
        memory.addCar(newCar.id)
    pass
    
def on_fullcarPositions(data):
    if gameTick != 0 or lastGameTick == 0:
        memory.addTick(gameTick)
    for eCar in data:
        id = None
        if "id" in eCar:
            if "name" in eCar["id"]:
                id = eCar["id"]["name"]
                
        if not id or not id in cars:
            print "Ignoring ID = " + id
            continue

        car = cars[id]
        if 'angleOffset' in eCar:
            car.angle = eCar['angleOffset']
        lastPosition = 0.0
        lastIndex = 0
        if 'piecePosition' in eCar:

            if "pieceIndex" in car.piecePosition:
                lastPosition = car.piecePosition['inPieceDistance']
                lastIndex = car.piecePosition['pieceIndex']
           
            car.piecePosition = eCar['piecePosition']
            car.pieceIndex = car.piecePosition['pieceIndex']
            car.currentTrackPiece = track.pieces[car.piecePosition['pieceIndex']]
                      
            if "pieceIndex" in car.piecePosition:
                if car.piecePosition['pieceIndex'] == lastIndex:
                    # We handle lagging connection with delta
                    car.speed = (car.piecePosition['inPieceDistance'] - lastPosition) / 1
                else:
                    prev = car.currentTrackPiece.prev
                    if prev.switch:
                        if not prev.isCurve:
                            pass
                        else:
                            pass
                    else:
                        if not prev.isCurve:
                            car.speed = (car.piecePosition['inPieceDistance'] + (prev.length - lastPosition)) / 1
                        else:
                            car.speed = (car.piecePosition['inPieceDistance'] + (prev.laneLength(track.lanes[car.piecePosition['lane']['startLaneIndex']]['distanceFromCenter']) - lastPosition)) / 1
                    
            
                                   
        if 'lap' in eCar:
            car.lap = eCar['lap']
            
        if car.pieceIndex in maxSpeeds:
            if car.speed > maxSpeeds[car.pieceIndex]:
                maxSpeeds[car.pieceIndex] = car.speed
        else:
            maxSpeeds[car.pieceIndex] = car.speed
            
        if gameTick != 0:
            memory.memorizeCar(id, car.speed, car.angle, lastIndex, car.piecePosition['inPieceDistance'])
                
    pass
    
def on_carVelocities(data):
    return
    i = 0
    xVel = data[0]['x']
    yVel = data[0]['y']
    if gameTick != 0:
        memory.memorizeCarVelocity(0, math.sqrt(xVel*xVel + yVel*yVel))
    
    pass
    
def on_liveRanking(data):
    pass 

def on_turboStart(data):
    turbos.append(gameTick)
    
    
def on_crash(data):
    id = None
    if "name" in data:
        id = data["name"]
            
    if not id or not id in cars:
        return

    car = cars[id]
    global track

    print car.speed
    print car.pieceIndex
    piece = track.pieces[car.pieceIndex]
    if not piece.isCurve:
        piece = track.pieces[(car.pieceIndex - 1) % len(track.pieces)]
    
    print gameTick
    print "Crashed with velocity %s on Curve (%s): alpha = %s, radius = %s" % (car.speed, car.pieceIndex, piece.angle, piece.radius)
    
    pass       
    
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: analyzeGame.py filename")
    else:
        filename = sys.argv[1]
        
        f = open(filename, 'r')
        
        str = f.read()
        
        messages = json.loads(str)
        
        msg_map = {
            'gameInit': on_gameInit,
            'fullCarPositions': on_fullcarPositions,
            'carVelocities': on_carVelocities,
            'liveRanking': on_liveRanking,
            'turboStart' : on_turboStart,
            'crash' : on_crash,
        }
        
        for msg in messages:
            msg_type = 'NoType'
            data = None
            tick = 0
            if 'msgType' in msg:
                msg_type = msg['msgType']
            if 'data' in msg:
                data = msg['data']
            if 'gameTick' in msg:
                tick = msg['gameTick']
                
            if msg_type in msg_map:
                gameTick = tick
                msg_map[msg_type](data)
                lastGameTick = tick
            else:
                print("Got {0}".format(msg_type))
            
        f.close()
        
        memory.savePNG()
        
        filename = os.path.join(os.path.dirname(__file__), "result.txt")
        with open(filename, 'w') as outfile:
            json.dump(maxSpeeds, outfile)